<?php

namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundleConfiguration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Deplacement;

class DeplacementController extends Controller{

/**
* @Route("/create-deplacement")
*/

public function createDeplacement(Request $request)
{

$deplacement= new Deplacement();
$from=$this->createFormBuilder($deplacement)
->add('nom',TextType::class)
->add('prenom',TextType::class)
->add('EMail',EmailType::class)
->add('dateDepart',DateType::class)
->add('dateArrive',DateType::class)
->add('heureDepart',DateTimeType::class)
->add('heureArrive',DateTimeType::class)
->add('Enregistrement',SubmitType::class,array('label'=>'Nouveau'))
->getForm();

$form->handleRequest($request);

if($form->isSubmitted()){

$deplacement=$form->getData();
$em=$this->getDoctrine()->getManager();
$em->persist($deplacement);
$em->flush();
return $this->redirect('/view-deplacement/'.$deplacement->getIdDepalacement());

}
return $this-render('deplacement/editDeplacement.html.twig',array('form'=>$form->createView())
);
}

/**
* @Route("/view-deplacement/{idDeplacement}")
*/
public function viewDeplacement($idDeplacement){
$deplacement=$this->getDoctrine()
->getRepository('AppBundle:Deplacement')
->find($idDeplacement);

if(!$deplacement){
throw $this->createNotFoundException('There are no deplacements with the following idDeplacement:'.$idDeplacement);
}

return $this->render('deplacement/viewDeplacement.html.twig',array('deplacement'=>$deplacement)
);
}

/** 
* @Route("show-deplacements")
*/

public function showAction(){
$deplacements=$this->getDoctrine()
->getRepository('AppBundle:Deplacement')
->findAll();

return $this->render('deplacements/show.html.twig',array('deplacements'=>$deplacements)
);
}


}


