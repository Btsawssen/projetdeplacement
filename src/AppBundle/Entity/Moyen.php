<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="moyen")
*/

class Moyen{

/**
* @ORM\Column(type="integer")
* @ORM\idMoyen
* @ORM\GenerateValue(strategy="AUTO")
*/

protected $idMoyen;


/**
* @ORM\Column(type="String",length=100)
*/

protected $nomMoyen;

/**
     * Get idMoyen
     *
     * @return int
     */
    public function getIdMoyen()
    {
        return $this->idMoyen;
    }
 

/**
     * Get nomMoyen
     *
     * @return string
     */
    public function getNomMoyen()
    {
        return $this->nomMoyen;
    }


}


?>


