<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
* @ORM\Entity
* @ORM\Table(name="deplacement")
*/

class Deplacement{

/**
* @ORM\Column(type="integer")
* @ORM\IdDeplacement
* @ORM\GeneratedValue(strategy="AUTO")
*/

protected $idDeplacement;

/** 
* @ORM\Column(type="string", length=100)
*/
protected $nom;

/**
* @ORM\Column(type="string", length=100)
*/

protected $prenom;

/**
* @ORM\Column(type="email")
*/

protected $Email;

/**
* @ORM\Column(type="date")
*/

protected $dateDepart;

/**
* @ORM\Column(type="date")
*/

protected $dateRetour;

/** 
* ORM\Column(type="heure", nullable=true)
*/

protected $heureDepart;

/** 
* ORM\Column(type="heure", nullable=true)
*/

protected $heureRetour;

/**
* @ORM\ManyToOne(targetEntity="src\AppBundle\Entity\Ville")
* @ORM\JoinColumn(nullable=false)
*/

protected $idVille;


/**
* @ORM\ManyToOne(targetEntity="src\AppBundle\Entity\Moyen")
* @ORM\JoinColumn(nullable=false)
*/

protected $idMoyen;

/**
     * Get id
     *
     * @return int
     */

    public function getIdDeplacement()
    {
        return $this->idDeplacement;
    }
 

/**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

/**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
 
}


?>

